import { createServer, Server } from 'http';
import * as express from 'express';
import * as socketIo from 'socket.io';

import { Message } from './models';

export class ChatServer {
    public static readonly PORT: number = 3000;
    private app: express.Application;
    private server: Server;
    private io: SocketIO.Server;
    private port: string | number;
    private usersList: object[];

    constructor() {
        this.createApp();
        this.config();
        this.createServer();
        this.sockets();
        this.listen();
    }

    private createApp(): void {
        this.app = express();
    }

    private createServer(): void {
        this.server = createServer(this.app);
    }

    private config(): void {
        this.port = process.env.PORT || ChatServer.PORT;
    }

    private sockets(): void {
        this.io = socketIo(this.server);
    }

    private getUsers(): object[] {
        this.usersList = [];
        const socketClients = this.io.sockets.clients(() => {});
        Object.keys(socketClients.sockets).forEach((id) => {
            if (socketClients.sockets[id].tsChatUser) {
                this.usersList.push(socketClients.sockets[id].tsChatUser);
            }
        });
        return this.usersList;
    }

    private listen(): void {
        this.server.listen(this.port, () => console.log('Running server on port ', this.port));

        this.io.on('connect', (socket: any) => {
            console.log('Connected client on port ', this.port);
            this.io.emit('usersList', this.getUsers());
            socket.on('message', (message: Message) => {
                console.log('message: ', JSON.stringify(message));
                socket.tsChatUser = message.from;
                this.io.emit('message', message);
            });
            socket.on('disconnect', () => {
                console.log('Client disconnected');
                this.io.emit('message', { from: socket.tsChatUser, action: 'left', context: '' });
            });
        });
    }

    public getApp(): express.Application {
        return this.app;
    }
}
