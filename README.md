# ChatTS

## Before start
Need to be installed: node.js, npm, gulp, ng

## Getting Started
To install all node_modules run the following command in server/client directories
```
$ npm i
```
### Structure
- server - server part (socket.io)
- client - client part (components, services, constants, utils)

## Main Commands
```
1. npm run server:i (to install server side modules)
2. npm run server:build (to run gulp command on server side)
3. npm run server:start (to start server from 'dist' directory)
4. npm run client:i (to install client side modules)
5. npm run client:start (to start client)
```
