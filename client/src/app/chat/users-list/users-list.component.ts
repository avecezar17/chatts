import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { User } from '../shared/model/user';
import { UserDialogType } from '../user-dialog/user-dialog-type';
import { SocketService } from '../shared/services/socket.service';

@Component({
  selector: 'chat-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})

export class UsersListComponent {
  @Input() users: User[] = [];
  @Input() currentUser: User;
  @Input() openUserPopup: Function;
  @Input() sendNotification: Function;
  @Output() onClickUserEvent = new EventEmitter<string>();

  constructor(private socketService: SocketService,
              public dialog: MatDialog) {}

  public onClickUserInfo() {
    this.openUserPopup({
      data: {
        username: this.currentUser.name,
        title: 'Edit Details',
        dialogType: UserDialogType.EDIT
      }
    });
  }

  public onClickUserMessage(name) {
    this.onClickUserEvent.emit(name);
  }
}
