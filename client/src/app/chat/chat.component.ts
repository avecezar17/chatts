import { Component, OnInit, ViewChildren, ViewChild, AfterViewInit, QueryList, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MatList, MatListItem } from '@angular/material';

import { Action } from './shared/model/action';
import { Event } from './shared/model/event';
import { Message } from './shared/model/message';
import { User } from './shared/model/user';
import { SocketService } from './shared/services/socket.service';
import { UserDialogComponent } from './user-dialog/user-dialog.component';
import { UserDialogType } from './user-dialog/user-dialog-type';

import getRandomId from '../../utils/randomId';

import API from '../../constants/api';

@Component({
  selector: 'chat-main',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})

export class ChatComponent implements OnInit, AfterViewInit {
  action = Action;
  currentUser: User;
  users: User[] = [];
  messages: Message[] = [];
  messageContent: string;
  ioConnection: any;
  dialogRef: MatDialogRef<UserDialogComponent> | null;
  defaultDialogUserParams: any = {
    disableClose: true,
    data: {
      title: 'Welcome',
      dialogType: UserDialogType.NEW
    }
  };

  // getting a reference to the overall list, which is the parent container of the list items
  @ViewChild(MatList, { read: ElementRef }) matList: ElementRef;

  // getting a reference to the items/messages within the list
  @ViewChildren(MatListItem, { read: ElementRef }) matListItems: QueryList<MatListItem>;

  constructor(private socketService: SocketService,
    public dialog: MatDialog) {}

  ngOnInit(): void {
    this.initModel();
    setTimeout(() => {
      this.openUserPopup(this.defaultDialogUserParams);
    }, 0);
  }

  ngAfterViewInit(): void {
    // subscribing to any changes in the list of items / messages
    this.matListItems.changes.subscribe(elements => {
      this.scrollToBottom();
    });
  }

  private scrollToBottom(): void {
    try {
      this.matList.nativeElement.scrollTop = this.matList.nativeElement.scrollHeight;
    } catch (err) {
    }
  }

  private initModel(): void {
    const randomId = getRandomId();
    this.currentUser = {
      id: randomId,
      avatar: `${API.AVATAR_URL}/${randomId}.png`
    };
  }

  private initIoConnection(): void {
    this.socketService.initSocket();

    this.socketService.usersList()
      .subscribe((users: User[]) => {
        this.users = users;
      });

    this.ioConnection = this.socketService.onMessage()
      .subscribe((message: Message) => {
        if (message.action) {
          switch (message.action) {
            case Action.JOINED:
              this.users.push({ ...message.from });
              break;
            case Action.LEFT:
              const index = this.users.findIndex((user) => user.id === message.from.id);
              this.users.splice(index, 1);
              break;
            case Action.RENAME:
              this.users.forEach((user) => {
                if (user.id === message.from.id) {
                  user.name = message.from.name;
                }
              });
              break;
            default: break;
          }
        }
        message.date = new Date(message.date).toLocaleString('en-US', {
          hour12: false,
          hour: 'numeric',
          minute: 'numeric'
        });
        this.messages.push(message);
      });


    this.socketService.onEvent(Event.CONNECT)
      .subscribe(() => this.sendNotification(this.currentUser, Action.JOINED));

    this.socketService.onEvent(Event.DISCONNECT)
      .subscribe(() => this.sendNotification(this.currentUser, Action.LEFT));
  }

  public onClickUserEvent(name) {
    const nameString = `@${name} `;
    if (!this.messageContent) {
      this.messageContent = nameString;
    }
    if (!this.messageContent.startsWith(nameString)) {
      this.messageContent = `@${name} ${this.messageContent}`;
    }
  }

  private openUserPopup(params): void {
    this.dialogRef = this.dialog.open(UserDialogComponent, params);
    this.dialogRef.afterClosed().subscribe(paramsDialog => {
      if (!paramsDialog) {
        return;
      }

      this.currentUser.name = paramsDialog.username;
      if (paramsDialog.dialogType === UserDialogType.NEW) {
        this.initIoConnection();
      } else if (paramsDialog.dialogType === UserDialogType.EDIT) {
        this.sendNotification(paramsDialog, Action.RENAME);
      }
    });
  }

  public sendMessage(message: string): void {
    if (!message) {
      return;
    }

    this.socketService.send({
      from: this.currentUser,
      content: message,
      date: Date.now(),
    });
    this.messageContent = null;
  }

  public sendNotification(params: any, action: Action): void {
    let message: Message;

    switch (action) {
      case Action.JOINED:
      case Action.LEFT:
        message = {
          from: this.currentUser,
          action: action
        };
        break;
      case Action.RENAME:
        message = {
          action: action,
          from: this.currentUser,
          content: {
            username: this.currentUser.name,
            previousUsername: params.previousUsername
          }
        };
        break;
      default: break;
    }

    this.socketService.send(message);
  }
}
