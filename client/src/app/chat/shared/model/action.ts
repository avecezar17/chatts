export enum Action {
    JOINED = 'joined',
    LEFT = 'left',
    RENAME = 'rename'
}
