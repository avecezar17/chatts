import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Message } from '../model/message';
import { Event } from '../model/event';

import * as socketIo from 'socket.io-client';
import { User } from '../model/user';

import API from '../../../../constants/api';

@Injectable()
export class SocketService {
    private socket;

    public initSocket = () => this.socket = socketIo(API.SERVER_URL);

    public send = (message: Message) => this.socket.emit('message', message);

    public usersList = () => new Observable<User[]> (observer => {
        this.socket.on('usersList', (usersList) => observer.next(usersList));
    })

    public onMessage = () => new Observable<Message> (observer => {
        this.socket.on('message', (data: Message) => observer.next(data));
    })

    public onEvent = (event: Event) => new Observable<Event> (observer => {
        this.socket.on(event, () => observer.next());
    })
}
