const API = {
  SERVER_URL: 'http://localhost:3000',
  AVATAR_URL: 'https://api.adorable.io/avatars/285',
};

export default API;
